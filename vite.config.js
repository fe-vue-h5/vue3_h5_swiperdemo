import { defineConfig } from 'vite'
import { resolve } from "path"; // 主要用于alias文件路径别名
import vue from '@vitejs/plugin-vue'

function pathResolve(dir) {
  return resolve(__dirname, ".", dir);
}

// https://vitejs.dev/config/
export default defineConfig(({ command, mode }) => {
  return {
    // dev 独有配置
    base: '',
    resolve: {
      alias: {
        '@': pathResolve('src'),
        '@comps': pathResolve('src/components'),
        '@imgs': pathResolve('src/assets/images'),
        '@styles': pathResolve('src/assets/styles'),
        '@utils': pathResolve('src/uilts'),
        '@routes': pathResolve('/src/routes'),
        '@pages': pathResolve('/src/pages'),
      }
    },
    css: {
      preprocessorOptions: {
        scss: {
          // additionalData: `@import "normalize.css/normalize.css";` // 每个scss文件都会引用当前css
        }
      },
      postcss: {
        plugins: [
          require('postcss-px-to-viewport')({
            unitToConvert: 'px', //需要转换的单位，默认为"px"
            viewportWidth: 750, // 视窗的宽度，对应的是我们设计稿的宽度
            unitPrecision: 13, // 指定`px`转换为视窗单位值的小数位数（很多时候无法整除）
            propList: ['*'], // 能转化为vw的属性列表
            viewportUnit: 'vw', // 指定需要转换成的视窗单位，建议使用vw
            fontViewportUnit: 'vw', //字体使用的视口单位
            selectorBlackList: ['.ignore-',], //指定不转换为视窗单位的类，可以自定义，可以无限添加,建议定义一至两个通用的类名
            minPixelValue: 1, // 小于或等于`1px`不转换为视窗单位，你也可以设置为你想要的值
            mediaQuery: false, // 允许在媒体查询中转换`px`
            replace: true, //是否直接更换属性值，而不添加备用属性
            exclude: [], //忽略某些文件夹下的文件或特定文件，例如 'node_modules' 下的文件
            landscape: false, //是否添加根据 landscapeWidth 生成的媒体查询条件 @media (orientation: landscape)
            landscapeUnit: 'vw', //横屏时使用的单位
            landscapeWidth: 750 //横屏时使用的视口宽度
          }),
        ]
      }
    },
    plugins: [vue()],
    // 打包配置
    build: {
      outDir: 'dist', //指定输出路径
      assetsDir: 'assets', // 指定生成静态资源的存放路径
      minify: 'terser' // 混淆器，terser构建后文件体积更小
    },
    // 本地运行配置，及反向代理配置
    server: {
      cors: true, // 默认启用并允许任何源
      open: false, // 在服务器启动时自动在浏览器中打开应用程序
      //反向代理配置，注意rewrite写法，开始没看文档在这里踩了坑
      proxy: {
          '/api': {
              target: 'http://0.0.0.0:3000',   //代理接口
              changeOrigin: true,
              rewrite: (path) => path.replace(/^\/api/, '')
        }
      }
    }
  }
})