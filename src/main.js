import { createApp } from 'vue'
import router from './routes'
import App from './App.vue'
// normalize css
import 'normalize.css/normalize.css'
import '@styles/common.css';


createApp(App).use(router).mount('#app')
