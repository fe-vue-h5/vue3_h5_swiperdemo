import { createRouter, createWebHashHistory } from "vue-router";
import * as each from './each'
const routes = [
  {
    path:"/",
    name:"index",
    component:()=>import("@pages/index/index.vue"),
    beforeEnter: (to, from, next) => {
      // 局部 路由守卫
      // console.log(to, 'by index page enter');
      // console.log(from, 'by index page enter');
      next();
    }
  }
]

const router = createRouter({
  history: createWebHashHistory(),
  routes
});


// 全局 before 路由守卫 
router.beforeEach((to, from, next) => each.wholeBeforeEach(to, from, next));

// 全局 after 路由守卫 
router.afterEach((to, from) => each.wholeAfterEach(to, from));

export default router;